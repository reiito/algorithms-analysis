import java.io.PrintStream;
import java.util.*;

public class BstMultiset<T> extends Multiset<T>
{
  //BST Node Class
  private class Node {
    // node variables
    String key;
    Node left;
    Node right;
    int count;

    // node constructor
    public Node (String key) {
      this.key = key;
      left = null;
      right = null;
      count = 1;
    }

    // getters
    String getKey() { return key; }
    Node getLeft() { return left; }
    Node getRight() { return right; }
    int getCount() { return count; }

    // setters
    void setKey(String newKey) { key = newKey; }
    void setLeft(Node newNode) { left = newNode; }
    void setRight(Node newNode) { right = newNode; }
    void setCount(int newCount) { count = newCount; }

    // count change
    void incCount() { count++; }
    void decCount() { count--; }
  }

  protected Node mRoot; //root of tree

  // multiset constructor
  public BstMultiset() {
    mRoot = null;
  }

  // calls recursive add to the root and updates the tree
  public void add(T item) {
    mRoot = insert(mRoot, (String)item);
  } // end of add()

  // inserts a node into the tree
  private Node insert(Node root, String key) {
    if (root == null) { // checks if node is empty (end of tree)
      root = new Node(key); //adds new node
    }
    else if (root.getKey().compareTo(key) < 0) { //traverse left
      root.setLeft(insert(root.getLeft(), key));
    }
    else if (root.getKey().compareTo(key) > 0) { //traverse right
      root.setRight(insert(root.getRight(), key));
    }
    else { //already exists, add to instances
      root.incCount();
    }

    return root;
  } // end of insert()

  // searches and returns a node via recursion
  private Node searchNode(Node root, String key) {
    if (root == null) //nothing to be found
      return root;

    if (root.getKey().compareTo(key) < 0) //traverse left
      return searchNode(root.getLeft(), key);
    else if (root.getKey().compareTo(key) > 0) //traverse right
      return searchNode(root.getRight(), key);

    return root;
  }

  // searches for and returns the count of a node
  public int search(T item) {
    Node searched = searchNode(mRoot, (String)item);
    if (searched == null) //node doesn't exist
      return 0;

    return searched.getCount();
  } // end of search()

  // removes one node
  public void removeOne(T item) {
    Node searched = searchNode(mRoot, (String)item);
    if (searched == null) //node doesn't exist
      return;

    if (searched.getCount() > 1) //if there are more than one
      searched.decCount(); //de-increment
    else
      removeAll(item);
  } // end of removeOne()

  // remove all instances of the node
  public void removeAll(T item) {
    mRoot = deleteNode(mRoot, (String)item); //start for the root
  }

  // find the minimum of a node
  private Node minElement(Node root) {
    if (root.left == null)
      return root;
    else
      return minElement(root.left);
  }

  // delete all instances of a node (re-arranging the tree)
  private Node deleteNode(Node root, String key) {
    if (root == null) //no tree
      return null;

    // find node
    if (root.getKey().compareTo(key) < 0) //traverse left
      root.setLeft(deleteNode(root.getLeft(), key));
    else if (root.getKey().compareTo(key) > 0) //traverse right
      root.setRight(deleteNode(root.getRight(), key));
    else {
      // if searched node has both children
      if (root.getLeft() != null && root.getRight() != null) {
        // find min element from right
        Node minNodeForRight = minElement(root.getRight());
        //replace curr node with min node from right subtree
        root.setKey(minNodeForRight.getKey());
        //replace curr node count with min node count from right subtree
        root.setCount(minNodeForRight.getCount());
        // delete min node from right now
        root.setRight(deleteNode(root.getRight(), minNodeForRight.getKey()));
      }
      // if searched node has only left child
      else if (root.getLeft() != null)
        root = root.getLeft();
      // if searched node has only right child
      else if (root.getRight() != null)
        root = root.getRight();
      // if searched node do not have child
      else //node delete
        root = null;
    }
    return root;
  }

  //print tree from root
  public void print(PrintStream out) {
    printTree(mRoot, out);
  } // end of print()

  // recursive print in preorder
  private void printTree(Node root, PrintStream out) {
    if (root != null) {
      out.println((String)root.getKey()+printDelim+root.getCount());
      printTree(root.getLeft(), out);
      printTree(root.getRight(), out);
    }
  }

} // end of class BstMultiset
